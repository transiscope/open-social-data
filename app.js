require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const expressSession = require('express-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const { Issuer,Strategy } = require('openid-client');
const MongoClient = require('mongodb').MongoClient;
const fileUpload = require('express-fileupload');
const expressLayouts = require('express-ejs-layouts');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const providerRouter = require('./routes/eventProviders');
const eventRouter = require('./routes/events');
const logger = require('./logger');
const morgan = require('morgan');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.set('layout', 'layouts/main');

const morganMiddleware = morgan(
  ':method :url :status :res[content-length] - :response-time ms',
  {
    stream: {
      // Configure Morgan to use our custom logger with the http severity
      write: (message) => logger.http(message.trim()),
    },
  }
);
app.use(morganMiddleware);
app.use(express.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressSession({
    secret: process.env['SESSION_SECRET'],
    resave: false,
    saveUninitialized: true,
    store: new expressSession.MemoryStore()
}));
app.use(fileUpload());

// Initialize Passport
app.use(passport.initialize());
app.use(passport.authenticate('session'));

Issuer.discover(process.env['ISSUER_URL']).then(function (oidcIssuer) {
  const client = new oidcIssuer.Client({
    client_id: process.env['CLIENT_ID'],
    client_secret: process.env['CLIENT_SECRET'],
    redirect_uris: [process.env['BASE_URL'] + '/auth'],
    post_logout_redirect_uris: [process.env['BASE_URL']]
  });

  // Store the client and issuer in app.locals for later use in logout
  app.locals.oidcClient = client;
  app.locals.oidcIssuer = oidcIssuer;

  // TODO : ça n'a pas l'air de fonctionner, trouver un moyer de specifier le scope
  client.authorizationUrl({scope: 'openid profile email'});

  passport.use('oidc', new Strategy({client}, (tokenSet, userinfo, done)=>{
    return done(null, {
      ...tokenSet.claims(),
      id_token: tokenSet.id_token
    });
  }));
});

// Configure Passport authenticated session persistence.
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

// Define routes.
app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/providers', providerRouter);
app.use('/events', eventRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.enable("trust proxy");

//MongoDB connexion
const mongoUri = "mongodb+srv://" + process.env['MONGODB_USER'] + ":" + process.env['MONGODB_PWD'] + "@" + process.env['MONGODB_URI'] + "?retryWrites=true&w=majority";
const mongoClient = new MongoClient(mongoUri, {useNewUrlParser: true});
mongoClient.connect()
    .catch(error => console.error(`Erreur de connexion à la base de donnée: ${error}`))
    .then(function (client) {
        app.locals.db = client.db("open-social-data");
    });

module.exports = app;
