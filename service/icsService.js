const kindOf = require("kind-of");
const ics = require('ics');
const eventMapper = require('../service/eventMapper');

module.exports = {
    updateIcsEvents: function (db, url, userid, events, next) {

        let collection = db.collection("Providers");
        let query = {
            url: url,
            type: 'Fichier ICS',
            userid: userid
        }
        const update = {
            $set: query
        };

        collection.findOneAndUpdate(query, update, {upsert: true})
            .then(function (data) {
                let providerId;
                if (data.value) {
                    providerId = data.value._id;
                } else {
                    providerId = data.lastErrorObject.upserted;
                }

                query = {
                    providerId: providerId
                }
                collection = db.collection("Events");

                collection.deleteMany(query)
                    .then(function () {
                        const eventsToInsert = [];
                        Object.values(events).forEach(async function (event) {
                            if (event.type === 'VEVENT') {
                                const eventToInsert = eventMapper.convertToJsonLdEvent(event.summary, event.description, event.start, event.end, event.location, event.url);
                                eventToInsert.providerId = providerId;
                                eventsToInsert.push(eventToInsert);
                            }
                        });

                        collection.insertMany(eventsToInsert)
                            .then(function () {
                                return eventsToInsert.length;
                            })
                            .catch(function (error) {
                                console.error(`Erreur d'écriture en base: ${error}`);
                                if (next) {
                                    return next(error);
                                }
                            });
                    })
                    .catch(function (error) {
                        console.error(`Erreur de suppression en base: ${error}`);
                        if (next) {
                            return next(error);
                        }
                    });
            })
            .catch(function (error) {
                console.error(`Erreur d'écriture en base: ${error}`);
                if (next) {
                    return next(error);
                }
            });
    },
    createIcsFile: function (jsonLdEvents) {
        const icsEvents = [];
        for (const event of jsonLdEvents) {
            const startDate = new Date(event.startDate);
            const endDate = new Date(event.endDate);
            let location = event.location;
            if (kindOf(location) === 'object') {
                location = location.streetAddress + ' ' + location.addressLocality;
            }

            const icsEvent = {
                title: event.name,
                description: event.description,
                start: [startDate.getFullYear(), startDate.getMonth(), startDate.getDay(), startDate.getHours(), startDate.getMinutes()],
                end: [endDate.getFullYear(), endDate.getMonth(), endDate.getDay(), endDate.getHours(), endDate.getMinutes()],
                location: location
            };
            if (event.url) {
                icsEvent.url = event.url;
            }

            icsEvents.push(icsEvent);
        }

        const {error, value} = ics.createEvents(icsEvents);

        if (error) {
            console.error(`Erreur lors de la création de données ICS: ${error}`);
            return error;
        } else {
            return value;
        }
    }
}