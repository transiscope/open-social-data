const axios = require('axios');
const ical = require('node-ical');
const eventMapper = require('../service/eventMapper');
const mobilizonService = require("./mobilizonService");

module.exports = {
    getAllEvents: function (db, userId) {
        return new Promise((resolve, reject) => {
            const projection = {
                type: true,
                url: true,
                group: true
            };
            db.collection("Providers").find({userid: userId}, {projection: projection}).toArray()
                .then(async function (providerList) {
                    const events = [];
                    for (const provider of providerList) {
                        switch (provider.type) {
                            case 'Mobilizon' :
                                events.push(...(await mobilizonService.getMobilizonEvents(provider)).events);
                                break;
                            case 'Fichier ICS' :
                                let eventCollection = db.collection("Events");
                                events.push(...await eventCollection.find({providerId: provider._id}).toArray());
                                break;
                            case 'URL ICS' :
                                try {
                                    const res = await axios.get(provider.url);
                                    const icsEvents = ical.sync.parseICS(res.data.toString());
                                    for (const event of Object.values(icsEvents)) {
                                        if (event.type === 'VEVENT') {
                                            events.push(eventMapper.convertToJsonLdEvent(event.summary, event.description, event.start, event.end, event.location));
                                        }
                                    }
                                } catch (error) {
                                    console.error(`Erreur lors de la tentative de requette HTTP vers l'URL ${provider.url} : ${error}`);
                                }
                                break;
                        }
                    }
                    resolve(events);
                })
                .catch(function (error) {
                    console.error(`Erreur de lecture en base: ${error}`);
                    reject(error);
                });
        });
    }
}