const mobilizonService = require('./mobilizonService');
const axios = require('axios');
const ical = require('node-ical');
const eventMapper = require('./eventMapper');

module.exports = {
    getProviderEvents: async function(providerId, db) {
        let events = [];
        let groupName = null;

        // Fetch provider from database using the passed db instance
        const provider = await db.collection("Providers").findOne({ _id: providerId });
        if (!provider) {
            throw new Error(`Provider avec l'ID ${providerId} non trouvé`);
        }

        switch (provider.type) {
            case 'Mobilizon':
                const groupEvents = await mobilizonService.getMobilizonEvents(provider);
                events = groupEvents.events;
                groupName = groupEvents.groupName;
                break;

            case 'Fichier ICS':
                events = await provider.db.collection("Events")
                    .find({ providerId: provider._id })
                    .toArray();
                break;

            case 'URL ICS':
                try {
                    const response = await axios.get(provider.url);
                    const icsEvents = ical.sync.parseICS(response.data.toString());
                    
                    for (const event of Object.values(icsEvents)) {
                        if (event.type === 'VEVENT') {
                            events.push(eventMapper.convertToJsonLdEvent(
                                event.summary,
                                event.description,
                                event.start,
                                event.end,
                                event.location
                            ));
                        }
                    }
                } catch (error) {
                    console.error(`Erreur lors de la tentative de requête HTTP vers l'URL ${provider.url} : ${error}`);
                }
                break;
        }

        return {
            events,
            groupName
        };
    }
}; 