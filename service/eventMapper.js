const kindOf = require("kind-of");

module.exports = {
    convertToJsonLdEvent: function (name, description, startDate, endDate, location, url) {
        if (kindOf(location) === 'object') {
            location = {
                "@context": "https://schema.org",
                "@type": "PostalAddress",
                addressLocality: location.locality,
                streetAddress: location.description
            };
        }
        return {
            "@context": "https://schema.org",
            "@type": "Event",
            name: name,
            description: description,
            startDate: startDate,
            endDate: endDate,
            location: location,
            url: url
        };
    }
}