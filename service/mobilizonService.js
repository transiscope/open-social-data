const url = require('url');
const {request, gql} = require('graphql-request');
const eventMapper = require('../service/eventMapper');

function getGroupEventsGraphqlQuery(group, page) {
    return gql`query {
                group (preferredUsername:"${group}") {
                    name
                    organizedEvents(limit: 1000, page: ${page}) {
                        elements {
                            title
                            url
                            physicalAddress {
                                description
                                locality
                            }
                            beginsOn
                            endsOn
                            description
                        }
                        total
                    }
                }
            }`
}

module.exports = {
    authenticate: function (mobilizonApp, res, next, db, userId) {
        const state = Math.random().toString(36).slice(2);
        const collection = db.collection("Users");
        const update = {
            $set: {
                userid: userId,
                state: state
            }
        };
        collection.updateOne({userid: userId}, update, {upsert: true})
            .then(function () {
                res.redirect(url.format({
                    pathname: mobilizonApp.url + '/oauth/authorize',
                    query: {
                        "client_id": mobilizonApp.client_id,
                        "redirect_uri": mobilizonApp.redirect_uri,
                        "scope": mobilizonApp.scope,
                        "state": state
                    }
                }));
            })
            .catch(function (error) {
                console.error(`Erreur d'écriture en base: ${error}`);
                return next(error);
            });
    },
    getMobilizonEvents: async function (provider) {
        let count = 0;
        let page = 1;
        let total = 1;
        const events = [];
        let groupResponse = {};
        const graphqlUrl = `${provider.url}/graphiql`;
        const result = {
            events: [],
            groupName: provider.group
        }

        //let organizedEvents = data.group.organizedEvents;
        while (count < total) {
            try {
                groupResponse = (await request(graphqlUrl, getGroupEventsGraphqlQuery(provider.group, page))).group;
            } catch (error) {
                console.error(`Erreur lors de la tentative de requette GraphQL vers l'URL ${provider.url} : ${error}`);
            }

            count += groupResponse.organizedEvents.elements.length;
            ++page;
            total = groupResponse.organizedEvents.total;
            events.push(...groupResponse.organizedEvents.elements);
            result.groupName = groupResponse.name;
        }

        for (const mobilizonEvent of events) {
            result.events.push(eventMapper.convertToJsonLdEvent(mobilizonEvent.title, mobilizonEvent.description, mobilizonEvent.beginsOn, mobilizonEvent.endsOn, mobilizonEvent.physicalAddress, mobilizonEvent.url));
        }

        return result;
    }
}