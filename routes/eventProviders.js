const {request, gql} = require('graphql-request');
const express = require('express');
const createError = require('http-errors');
const ical = require('node-ical');
const router = express.Router();
const axios = require('axios');
const icsService = require('../service/icsService');
const mobilizonService = require('../service/mobilizonService');

/* GET Mobilizon provider creation page */
router.get('/mobilizon',
    require('connect-ensure-login').ensureLoggedIn(),
    function (req, res) {
        res.render('mobilizonForm');
    }
);

router.post('/mobilizon',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        let providerUrl = req.body.providerUrl;
        let group = req.body.group;
        const secured = req.body.secured;

        if (!providerUrl) {
            res.render('mobilizonForm');
        }

        if (secured) {
            const collection = req.app.locals.db.collection("MobilizonApps");
            let mobilizonApp = await collection.findOne({url: providerUrl});
            if (!mobilizonApp) {
                const redirectUri = process.env['BASE_URL'] + 'providers/mobilizon/redirect';
                axios.post(providerUrl + "/apps", {
                    name: "Open Social Data",
                    redirect_uri: redirectUri,
                    website: process.env['BASE_URL'],
                    scope: "read:event"
                })
                    .then(function (response) {
                        mobilizonApp = {
                            url: providerUrl,
                            client_id: response.data.client_id,
                            client_secret: response.data.client_secret,
                            redirect_uri: response.data.redirect_uri[0],
                            scope: response.data.scope
                        };
                        collection.insertOne(mobilizonApp)
                            .then(function () {
                                mobilizonService.authenticate(mobilizonApp, res, next, req.app.locals.db, req.user.sub);
                            })
                            .catch(function (error) {
                                console.error(`Erreur d'écriture en base: ${error}`);
                                return next(error);
                            });
                    })
                    .catch(function (error) {
                        console.error(`Erreur dans le processus d'authentification vers le serveur Mobilizon: ${error}`);
                        res.redirect(`/user/profile?errorAuth=${providerUrl}`);
                    });
            } else {
                mobilizonService.authenticate(mobilizonApp, res, next, req.app.locals.db, req.user.sub);
            }
        } else {
            const query = gql`
            query {
                events (limit:1) {
                    elements {
                        id
                    }
                }
            }`;
            request(`${providerUrl}/graphiql`, query)
                .then(function () {
                    const collection = req.app.locals.db.collection("Providers");
                    
                    const document = {
                        url: providerUrl,
                        type: 'Mobilizon',
                        group: group,
                        name: req.body.name,
                        userid: req.user.sub
                    };

                    const operation = req.body.id
                        ? collection.updateOne(
                            { _id: new require('mongodb').ObjectId(req.body.id) },
                            { $set: document }
                          )
                        : collection.insertOne(document);

                    operation
                        .then(function () {
                            res.redirect(`/user/profile`);
                        })
                        .catch(function (error) {
                            console.error(`Erreur d'écriture en base: ${error}`);
                            return next(error);
                        });
                })
                .catch(function (error) {
                    console.error(`Erreur lors de la tentative de requette GraphQL vers l'URL ${providerUrl} : ${error}`);
                    res.redirect(`/user/profile?errorMobilizon=${providerUrl}`);
                });
        }
    }
);

/* GET Mobilizon redirect page */
router.get('/mobilizon/redirect',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        let collection = req.app.locals.db.collection("Users");
        const user = await collection.findOne({userid: req.user.sub, state: req.query.state});
        if (!user) {
            return next(createError(403));
        }

        collection = req.app.locals.db.collection("MobilizonApps");
        const mobilizonApp = await collection.findOne({client_id: req.query.client_id});
        if (!mobilizonApp) {
            console.error(`Erreur dans le processus d'authentification vers un serveur Mobilizon. client_id ${req.query.client_id} inconnu.`);
        }

        axios.post(mobilizonApp.url + "/oauth/token", {
            code: req.query.code,
            client_id: req.query.client_id,
            client_secret: mobilizonApp.client_secret,
            grant_type: "authorization_code",
            redirect_uri: mobilizonApp.redirect_uri,
            scope: mobilizonApp.scope
        })
            .then(function (response) {
                collection = req.app.locals.db.collection("Providers");
                const document = {
                    url: mobilizonApp.url,
                    type: 'Mobilizon',
                    userid: req.user.sub,
                    refresh_token: response.data.refresh_token
                };

                const operation = req.body.id
                    ? collection.updateOne(
                        { _id: new require('mongodb').ObjectId(req.body.id) },
                        { $set: document }
                      )
                    : collection.insertOne(document);

                operation
                    .then(function () {
                        res.redirect(`/user/profile`);
                    })
                    .catch(function (error) {
                        console.error(`Erreur d'écriture en base: ${error}`);
                        return next(error);
                    });
            })
            .catch(function (error) {
                console.error(`Erreur dans le processus d'authentification vers le serveur Mobilizon: ${error}`);
                res.redirect(`/user/profile?errorAuth=${mobilizonApp.url}`);
            });

        res.render('icsFileUploadForm');
    }
);

/* GET ICS file upload page */
router.get('/ics',
    require('connect-ensure-login').ensureLoggedIn(),
    function (req, res) {
        res.render('icsFileUploadForm');
    }
);

router.post('/ics',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        const icsFile = req.files.icsFile;

        if (!icsFile) {
            res.render('icsFileUploadForm');
        }

        if (icsFile.mimetype !== 'text/calendar') {
            res.redirect(`/user/profile?errorIcs=type de fichier non supporté`);
        }

        const events = ical.sync.parseICS(icsFile.data.toString());
        if (Object.keys(events).length <= 0) {
            res.redirect(`/user/profile?errorIcs=Aucune donnée ICS valide dans le fichier`);
        } else {
            await icsService.updateIcsEvents(req.app.locals.db, icsFile.name, req.user.sub, events, next);
            res.redirect(`/user/profile`);
        }
    }
);

/* GET ICS URL download page */
router.get('/icsUrl',
    require('connect-ensure-login').ensureLoggedIn(),
    function (req, res) {
        res.render('icsUrlForm');
    }
);

router.post('/icsUrl',
    require('connect-ensure-login').ensureLoggedIn(),
    function (req, res, next) {
        let icsUrl = req.body.icsUrl;

        if (!icsUrl) {
            res.render('icsUrlForm');
        }

        axios.get(icsUrl)
            .then(async function (response) {
                const events = ical.sync.parseICS(response.data.toString());
                if (Object.keys(events).length <= 0) {
                    res.redirect(`/user/profile?errorIcs=Aucune donnée ICS valide via le lien fourni`);
                } else {
                    const collection = req.app.locals.db.collection("Providers");
                    const document = {
                        url: icsUrl,
                        name: req.body.name,
                        type: 'URL ICS',
                        userid: req.user.sub
                    };

                    const operation = req.body.id
                        ? collection.updateOne(
                            { _id: new require('mongodb').ObjectId(req.body.id) },
                            { $set: document }
                          )
                        : collection.insertOne(document);

                    operation
                        .then(function () {
                            res.redirect(`/user/profile`);
                        })
                        .catch(function (error) {
                            console.error(`Erreur d'écriture en base: ${error}`);
                            return next(error);
                        });
                }
            })
            .catch(function (error) {
                console.error(`Erreur de récupération du fichier ICS: ${error}`);
                return next(error);
            });
    }
);

module.exports = router;