const express = require('express');
const router = express.Router();
const createError = require('http-errors');
const jwt = require('jsonwebtoken');
const eventService = require('../service/eventService');
const icsService = require('../service/icsService');
const providerService = require('../service/providerService');
const ObjectID = require('mongodb').ObjectID;

module.exports = router;

/* GET events of the corresponding JWT token */
router.get('/', function (req, res, next) {
    const token = req.query.accessToken;
    if (!token) {
        return next(createError(401));
    }

    jwt.verify(token, process.env['JWT_SECRET'], async function (err, decoded) {
        if (err) {
            console.error(`Token invalide : ${err}`)
            return next(createError(401));
        }

        let collection = req.app.locals.db.collection("Users");
        const user = await collection.findOne({token: token});

        if (!user) {
            return next(createError(403));
        }

        let jsonFormat = true;
        if (req.get('Accept').includes('text/calendar')) {
            jsonFormat = false;
        }
        eventService.getAllEvents(req.app.locals.db, decoded.userid)
            .then(function (events) {
                if (jsonFormat) {
                    res.json(events);
                    res.end();
                } else {
                    res.set('Content-Type', 'text/calendar');
                    res.send(icsService.createIcsFile(events));
                    res.end();
                }
            })
            .catch(function (error) {
                return next(error);
            });
    });
});

router.get('/:id',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        const apiCollection = req.app.locals.db.collection("API");
        const api = await apiCollection.findOne({ _id: ObjectID.createFromHexString(req.params.id) });
        const events = [];  
        for (const providerId of api.selectedProviders) {
            const { events: providerEvents, groupName   } = await providerService.getProviderEvents(ObjectID.createFromHexString(providerId), req.app.locals.db);
            events.push(...providerEvents);
        }  
        res.json(events);
    }
)   

/* GET events of the corresponding JWT token as ICS format */
router.get('/ics', function (req, res, next) {
    const token = req.query.accessToken;
    if (!token) {
        return next(createError(401));
    }

    jwt.verify(token, process.env['JWT_SECRET'], async function (err, decoded) {
        if (err) {
            console.error(`Token invalide : ${err}`)
            return next(createError(401));
        }

        let collection = req.app.locals.db.collection("Users");
        const user = await collection.findOne({token: token});

        if (!user) {
            return next(createError(403));
        }
        eventService.getAllEvents(req.app.locals.db, decoded.userid)
            .then(function (events) {
                res.set('Content-Type', 'text/calendar');
                res.send(icsService.createIcsFile(events));
                res.end();
            })
            .catch(function (error) {
                return next(error);
            });
    });
});