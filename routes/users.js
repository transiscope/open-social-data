const express = require('express');
const router = express.Router();
const ObjectID = require('mongodb').ObjectID;
const jwt = require('jsonwebtoken');
const axios = require('axios');
const ical = require('node-ical');
const mobilizonService = require('../service/mobilizonService');
const eventMapper = require('../service/eventMapper');
const providerService = require('../service/providerService');

/* GET users profile */
router.get('/profile',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        try {
            let userCollection = req.app.locals.db.collection("Users");
            const query = { userid: req.user.sub };
            let projection = {
                token: true
            };

            const dbDoc = await userCollection.findOne(query, { projection: projection });
            let userToken;
            if (dbDoc) {
                userToken = dbDoc.token;
            }

            const providerCollection = req.app.locals.db.collection("Providers");
            projection = {
                _id: true,
                url: true,
                type: true,
                name: true
            };
            const providers = await providerCollection.find({ userid: req.user.sub }, { projection: projection }).toArray();

            const apiCollection = req.app.locals.db.collection("API");

            const apis = await apiCollection.find({ userid: req.user.sub }, {
                name: true,
                selectedProviders: true
            }).toArray();

            apis.forEach(api => {
                api.selectedProviders = api.selectedProviders.map(providerId => {
                    return providers.find(provider => provider._id.toString() === providerId);
                });
            }); 

            res.render('profile', {
                user: req.user,
                apis: apis,
                providers: providers,
                errorAuth: req.query.errorAuth,
                errorMobilizon: req.query.errorMobilizon,
                errorIcs: req.query.errorIcs,
                deleteSuccess: req.query.deleteSuccess,
                token: userToken
            });
        } catch (error) {
            console.error(`Erreur de lecture en base: ${error}`);
            return next(error);
        }
    }
);

/* CREATE API */
router.post('/api',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        const apiCollection = req.app.locals.db.collection("API");
        const document = {
            userid: req.user.sub,
            name: req.body.name,
            selectedProviders: req.body.selectedProviders
        };


        const operation = req.body.id
            ? apiCollection.updateOne(
                { _id: new require('mongodb').ObjectId(req.body.id) },
                { $set: document }
              )
            : apiCollection.insertOne(document);

        operation
            .then(function () {
                res.redirect(`/user/profile`);
            })
            .catch(function (error) {
                console.error(`Erreur d'écriture en base: ${error}`);
                return next(error);
            });
    }
);

router.get('/api/:id/delete',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        const apiCollection = req.app.locals.db.collection("API");
        const apis = await apiCollection.deleteOne({ _id: ObjectID.createFromHexString(req.params.id) });
        res.redirect(`/user/profile`);
    }
)

router.get('/api/:id/edit',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        const apiCollection = req.app.locals.db.collection("API");
        const api = await apiCollection.findOne(
            { _id: ObjectID.createFromHexString(req.params.id) },
            {
                name: true,
                selectedProviders: true
            }
        );
        const providerCollection = req.app.locals.db.collection("Providers");       
        const providers = await providerCollection.find({ userid: req.user.sub }, {
            _id: true,
            url: true,
            type: true,
            name: true
        }).toArray();

        res.render('api', { api: api, providers: providers });
    }
)

router.get('/api/create',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        const providerCollection = req.app.locals.db.collection("Providers");
        const providers = await providerCollection.find({ userid: req.user.sub }, {
            _id: true,
            url: true,
            type: true,
            name: true
        }).toArray();

        res.render('api', {
            providers: providers
        });
    }
)

/* CREATE Access Token */
router.post('/token',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        let collection = req.app.locals.db.collection("Users");
        const query = { userid: req.user.sub };
        const dbDoc = await collection.findOne(query);
        if (!dbDoc) {
            const token = jwt.sign({ userid: req.user.sub }, process.env['JWT_SECRET']);
            collection = req.app.locals.db.collection("Users");
            collection.insertOne({ userid: req.user.sub, token: token, tokenTimestamp: Date.now() })
                .catch(function (error) {
                    console.error(`Erreur d'écriture en base: ${error}`);
                    return next(error);
                });
        }

        res.redirect(`/user/profile`);
    }
);

/* DELETE Access Token */
router.get('/token/delete',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        let collection = req.app.locals.db.collection("Users");
        await collection.deleteOne({ userid: req.user.sub })
            .catch(function (error) {
                console.error(`Erreur de suppression en base: ${error}`);
                return next(error);
            });

        res.redirect(`/user/profile`);
    }
);

/* GET event of a provider */
router.get('/apps/:id',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {
        try {
            const id = ObjectID.createFromHexString(req.params.id);
            const result = await providerService.getProviderEvents(id, req.app.locals.db);
            const provider = await req.app.locals.db.collection("Providers").findOne({ _id: id });

            res.render('provider', {
                events: result.events,
                group: result.groupName,
                providerUrl: provider.url,
                type: provider.type
            });

        } catch (error) {
            console.error(`Erreur lors de la récupération des événements: ${error}`);
            return next(error);
        }
    }
);

router.get('/apps/:id/edit',
    require('connect-ensure-login').ensureLoggedIn(),
    async function (req, res, next) {

        let id = ObjectID.createFromHexString(req.params.id);
        let collection = req.app.locals.db.collection("Providers");
        const provider = await collection.findOne({ _id: id });
        switch (provider.type) {
            case 'Mobilizon':
                res.render('mobilizonForm', {
                    provider: provider
                });
                break;
            case 'Fichier ICS':
                res.render('icsFileUploadForm', {
                    provider: provider
                });
                break;
            case 'URL ICS':
                res.render('icsUrlForm', {
                    provider: provider
                });
                break;
        }
    }
);

router.get('/apps/:id/delete',
    require('connect-ensure-login').ensureLoggedIn(),
    function (req, res, next) {
        if (req.params.id.length !== 24) {
            var error = new Error('Application inexistante.');
            error.status = 404;
            return next(error);
        }

        let id = ObjectID.createFromHexString(req.params.id);
        let collection = req.app.locals.db.collection("Providers");
        collection.findOne({ _id: id }, async function (error, provider) {
            if (error) {
                console.error(`Erreur lors de lecture en base: ${error}`);
                return next(error);
            }
            if (provider) {
                const query = {
                    providerId: provider._id
                }
                collection = req.app.locals.db.collection("Events");
                collection.deleteMany(query)
                    .then(function () {
                        collection = req.app.locals.db.collection("Providers");
                        collection.deleteOne({ _id: id })
                            .then(function () {
                                res.redirect(`/user/profile?deleteSuccess=${provider.url}`);
                            })
                            .catch(function (error) {
                                console.error(`Erreur de suppression en base: ${error}`);
                                return next(error);
                            });
                    })
                    .catch(function (error) {
                        console.error(`Erreur de suppression en base: ${error}`);
                        return next(error);
                    });
            }
        });
    }
);


module.exports = router;