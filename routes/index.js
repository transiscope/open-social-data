const express = require('express');
const router = express.Router();
const passport = require('passport');

router.use(passport.initialize());
router.use(passport.session());

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {user: req.user, error: req.query.error, error_auth: req.query.error_auth});
});

router.get('/privacy', function (req, res, next) {
    res.render('privacy');
});

/* Auth routes */
router.get('/login', passport.authenticate('oidc'));

router.get('/auth', passport.authenticate('oidc', { successRedirect: '/user/profile', failureRedirect: '/'}));

router.get('/logout',require('connect-ensure-login').ensureLoggedIn(), (req, res) => {
    // Get id_token from session if available
    const id_token = req.user?.id_token;
    // Clear local session
    req.logout(() => {
      req.session.destroy(() => {
        // Construct the OIDC end session URL
        const endSessionUrl = req.app.locals.oidcIssuer.end_session_endpoint;
        if (endSessionUrl && id_token) {
          // Redirect to OIDC provider's logout endpoint
          const params = new URLSearchParams({
            id_token_hint: id_token,
            post_logout_redirect_uri: process.env['BASE_URL']
          });
          res.redirect(`${endSessionUrl}?${params.toString()}`);
        } else {
          // Fallback if no end session endpoint is available
          res.redirect('/');
        }
      });
    });
  });

module.exports = router;
