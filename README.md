# open-social-data

## Présentation

Projet pour mutualiser les données d'événements depuis différentes sources et les rendre accessibles via une API.

## Installation

* NodeJS et npm doivent être installé afin de télécharger les dépendances du projet et d'exécuter l'application.
* Télécharger les sources via Git.
* Se placer dans le repertoire des sources.
* Exécuter la commande npm install.

## Configuration

* Configurer le serveur d'authentification via OIDC pour pointer vers le serveur d'installation et récupérer les
  variables CLIENT_ID et CLIENT_SECRET
* Créer une base de donnée MongoDB avec un compte permettant de s'y connecter.
* Créer un fichier nommé .env dans le repertoire du projet contenant les variables d'environnement :
    * BASE_URL qui contient l'URL racine qui permet d'accéder à l'application.
    * SESSION_SECRET un secret pour la gestion des sessions web.
    * ISSUER_URL, CLIENT_ID et CLIENT_SECRET par rapport au serveur d'authentification précédemment configuré.
    * MONGODB_URI, MONGODB_USER et MONGODB_PWD par rapport à la base de donnée MongoDB précédemment créée.
    * JWT_SECRET un secret pour les clés d'authentification générées.

## Execution

* Une fois l'application configurée, lancer l'application :
    * npm start
    * ou yarn start
    * ou exécuter le fichier bin/www grâce à NodeJS : node bin/www .
